# 1. Normativa sui dati

La normativa descrive tutte le informazioni che vengono utilizzate 
dall'applicazione **Swish** per fornire tutti i servizi e le funzionalità necessarie.

---

## Quali informazioni raccogliamo?

Per poter utilizzare l'applicazione dobbiamo trattare alcune informazioni che ti riguardano.

### Dati forniti

Tutte le informazioni che fornisci quando utilizzi l'applicazione vengono raccolte: 
dati forniti durante la registrazione, durante il login attraverso provider come Facebook o Google.
Possono trattarsi anche di foto che possono essere caricate come immagine di profilo.

Al momento non raccogliamo nessun dato di tipo speciale che sono soggette a protezioni speciali ai sensi
della legge UE.

### Rete di utenti

Raccogliamo informazioni su utenti con cui ti connetti, esempio quando decidi di seguire o smettere
di seguire un utente.

### Utilizzo

Raccogliamo informazioni su come utilizzi l'applicazione, quando scrivi un post, lo commenti o lo apprezzi.
Queste informazioni vengono sempre integrate con la data e l'ora in cui viene compiuta l'azione.

---

## Come usiamo le informazioni?

### Funzionalità dell'applicazione

Le informazioni che ci fornite le usiamo per supportare le funzionalità dell'applicazione: visualizzare
la tua feed, visualizzare gli ultimi post pubblicati.
Visualizzare il profilo di un utente con le informazioni relative come i post che ha pubblicato, il numero 
di utenti seguiti e di quelli che lo seguono.
Visualizzare i post con le relative informazioni come i commenti e il numero di apprezzamenti ricevuti.

### Inserzioni

L'applicazione fa uso di servizi di terze parti come AdMob per l'inserimento di inserzioni selezionate 
in base all'utente. Per sapere di più su quali informazioni vengono utilizzate dal servizio, consulare
il servizio di **Google AdMob**.

---

## Come vengono condivise le informazioni?

Possiamo suddividere le informazioni in macro categorie: pubbliche, private, anonime.

### Informazioni pubbliche

Le informazioni pubbliche sono le informazioni che sono visibili da tutte gli utenti.

###### Profilo

* Nome utente
* Immagine del profilo, se fornita
* Numero post pubblicati
* Numero dei seguaci
* Numero degli utenti seguiti
* Post pubblici con numero di mi piace e numero di commenti

###### Post

* Testo del post
* Hashtags  
* Nome utente se il post è pubblico
* Numero di mi piace
* Commenti pubblicati

### Informazioni private

Le informazioni private sono informazioni che possono essere visualizzate solo da te.

###### Profilo

Nel tuo profilo puoi vedere i post che hai pubblicato in anonimo. Nelle impostazioni puoi visualizzare
le lingue che hai selezionato come preferite.

### Informazioni anonime

Quando crei un post puoi crearlo in maniera anonima, cioè il post è visibile tra gli ultimi post pubblicati
ma non è riconducibile a chi l'ha creato. Questi post sono visibili nella pagina del profilo solo dai 
proprietari.

### Informazioni condivisibili sui social

Tutti i post pubblicati sono condivisibili dagli utenti e possono essere scaricati come immagine nel
proprio dispositivo.

### Informazioni di cui teniamo traccia

Swish tiene traccia dell'ultimo login effettuato solo per scopo di analisi.

---

## Conservazione dei dati e cancellazione account

Tutti i dati vengono mantenuti fino a quando non decidi di eliminarli.
Quando elimini il tuo account, rimuoviamo anche tutti i dati ad esso associati. 
L'eliminazione è irreversibile, quindi una volta confermata la richiesta di eliminazione non potrai più
accedere al tuo account e ripristinare i vecchi dati.
L'eliminazione può essere effettuata andando nelle impostazioni del proprio profilo e cliccando
il bottone di eliminazione.

---

## Dove risiedono i miei dati

L'applicazione si appoggia ai servizi di **Google Firebase**. Le informazioni di registrazione e accesso
sono gestite da **Firebase Auth**, mentre tutte le altre informazioni sono gestire dalla suite Firebase
nella regione EUW. Per ulteriori informazioni dai un'occhiata a Google Firebase e ai loro 
termini di privacy.

---

---

# 2. Condizioni di utilizzo

**Benvenuto su Swish!** Swish ti permette di sfogarti, ispirare, strappare sorrisi o chiedere pareri in segreto
semplicemente pubblicado frasi.

Fai swipe per leggere le ultime frasi scritte dagli utenti in un lampo, metti mi piace e lascia un parere
con un commento. Segui i tuoi amici, gli Swisher che più ti piacciono e resta sempre aggiornato su cosa pubblicano.

Entra a far parte della community e inizia a pubblicare le tue idee!

## Supporto alla community

Swish si impegna a rilevare e punire usi impropri dell'appliazione da parte degli utenti e comportamenti dannosi nei 
confronti di altri utenti. Swish può ritenere necessario bloccare l'accesso ad un utente, rimuovere contenuti
o disabilitare funzionalità.

## Impegni dell'utente nei confronti della community

Per una sana community è necessario responsabilizzare i nostri utenti; per questo è anche necessario che
i contenuti siano riconducibili ad una persona. L'utente è tenuto a seguire le seguenti regole:

* Non condividere le credenziali di accesso;
* Creare uno e un solo account;
* Non condividere post che possano danneggiare, ferire o violare diritti di altri utenti;
* Non condividere contenuti a sfondo sessuale che possano ferire la sensibilità di altri utenti;
* Non condividere contenuti contro alla legge, ingannevoli, discriminatori o fraudolenti;  
* Rispettare le opinioni altrui;
* Rispettare le altre religioni;
* Rispettare le altre etnie;
* Non utilizzare l'applicazione per danneggiarla o creare malfunzionamenti.

In caso un utente non rispetti queste regole, 
Swish potrebbe bloccare o eliminare l'account senza nessun preavviso.

## Come ci finanziamo?

Utilizzando l'applicazione, l'utente accetta che Swish possa mostrare inserzioni suggerite e gestite 
dalla piattaforma di Google AdMob.

> **Come contattare Swish?** Visitando la scheda sviluppatore su Google Play troverai le informazioni necessarie per contattarci.


